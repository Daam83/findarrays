package findarray2;


import java.util.Arrays;

public class MyFindArray implements FindArray {

    @Override
    public int findArray(int[] array, int[] subArray) {

        int indexEquals = -1; // temporary variation basic set -1
        int[] tempArray = new int[subArray.length];


        if (subArray.length < array.length)//sub array is smaller than main array
        {
            for (int i = 0; i < array.length; i++) {

                for (int j = 0; j < subArray.length; j++) {

                    if (i < array.length - 1) {
                        tempArray[j] = array[i + j]; // create own copy of table array from i index
                    }

                }

                System.out.println(Arrays.toString(tempArray));
                System.out.println(i);

                if (Arrayequals(tempArray, subArray)) { // check if tables are te same
                    indexEquals = i;
                    System.out.println("table equals at " + indexEquals);


                }
                if (i == (array.length - 1)) {// if loop past to the last i then return index where the equals table begin.
                    return indexEquals;
                }


            }

        } else if (subArray.length == array.length)//else when the length of sub array is equal the length of main array
        {
            if (Arrayequals(subArray, array)) {
                return 0;
            }

        } else {
            return -1;
        }
        return -1;
    }

    //Main method
    public static void main(String[] args) {
        MyFindArray objFindArray = new MyFindArray();
        int[] array = {4, 9, 3, 7, 8, 3, 7, 1, 9, 9, 0};
        int[] sub_array = {3, 7};
        System.out.println(objFindArray.findArray(array, sub_array));
    }

    private static boolean Arrayequals(int[] a, int[] a2) { // metod coped from arrays.JAVA
        if (a == a2)
            return true;
        if (a == null || a2 == null)
            return false;

        int length = a.length;
        if (a2.length != length)
            return false;

        for (int i = 0; i < length; i++)
            if (a[i] != a2[i])
                return false;

        return true;
    }
}